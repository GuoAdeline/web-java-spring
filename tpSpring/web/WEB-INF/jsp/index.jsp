<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">-->
<!--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <p>Hello! This is the default welcome page for a Spring Web MVC project.</p>
        <p><i>To display a different welcome page for this project, modify</i>
            <tt>index.jsp</tt> <i>, or create your own welcome page then change
                the redirection in</i> <tt>redirect.jsp</tt> <i>to point to the new
                welcome page and also update the welcome-file setting in</i>
            <tt>web.xml</tt>.</p>
    </body>
</html>-->
<!DOCTYPE html>
<html lang="fr-fr">
    <head>
        <title>AuctionMan Connection Page</title>
        <link rel="stylesheet" type="text/css" media="screen" href="css/login.css">
        
    </head>
    <body>
        <form:form action="listitems.do" method="POST" >
            <h1>Auction Login</h1>
            <p><input type="text" name="login" /></p>
            <p><input type="password" name="passw" /></p>
            <p><button type="submit">Login</button></p>
        </form:form>
    </body>
</html>


