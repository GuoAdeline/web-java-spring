<%-- 
    Document   : hello
    Created on : 2019-2-15, 11:03:18
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <!--    <h1>Hello World! <c:out value="${firstName}"/> <c:out value="${lastName}"/>!</h1>
        <c:forEach var="aString" items="${liste}">
            <p><c:out value="${aString}" /></p>
        </c:forEach>
    -->
        
        <h1>Hello World <c:out value="${aUser.getLogin()}"/> <c:out value="${aUser['passw']}"/>!</h1>
    </body>
       
</html>
